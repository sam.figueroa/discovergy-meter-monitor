# frozen_string_literal: true

require 'rubygems'
require 'bundler/setup'
require 'dotenv/load'
require 'pry'

require './lib/meter_monitor'

DEBUG = ENV.fetch 'DEBUG', false

puts "Waiting for messages...(ctrl+c to abort)"

MeterMonitor.new.monitor_power_meter

