# frozen_string_literal: true

require 'minitest/unit'
require 'minitest/autorun'
require "minitest/reporters"
require 'mocha/minitest'

Minitest::Reporters.use! Minitest::Reporters::ProgressReporter.new({ color: true })
