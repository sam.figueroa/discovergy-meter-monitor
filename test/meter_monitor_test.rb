# frozen_string_literal: true

require 'test_helper'
require_relative '../lib/meter_monitor'

class MonitorTest < Minitest::Test
  FIXTURE_MQTT = "{\"time\":222,\"latest\":{\"power_w\":666}}"

  def setup
    @subject ||= MeterMonitor.new
  end

  def test_initialize
    assert @subject
    assert_instance_of Cache, @subject.cache
    assert_instance_of Notify, @subject.notification_service
  end

  def test_monitor_power_meter
    MQTT::Client.stubs connect: Proc

    @subject.monitor_power_meter
  end

  def test_poll_mqtt_with_unreachable_host
    @subject.stubs host: 'mumbo_jumbo'

    assert_equal @subject.host, 'mumbo_jumbo'
    assert_raises(SocketError) { @subject.poll_mqtt }
  end

  def skip_test_poll_mqtt_with_unreachable_user
    @subject.stubs user: 'mumbo_jumbo'

    assert_equal @subject.user, 'mumbo_jumbo'
    assert_raises(MeterMonitor::NetworkException) { @subject.monitor_power_meter }
  end

  def test_process_message_when_diff
    Notify.any_instance.expects(:execute).never
    @subject.cache.expects(:diff?).once.returns true

    @subject.process_message FIXTURE_MQTT
    assert_equal @subject.cache.count, 1
    assert_equal @subject.cache.first.power, 666
  end

  def test_process_message_when_no_diff
    Notify.any_instance.expects(:execute).once
    @subject.cache.expects(:diff?).once.returns false

    @subject.process_message FIXTURE_MQTT
    assert_equal @subject.cache.count, 1
  end
end
