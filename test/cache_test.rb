# frozen_string_literal: true

require 'test_helper'
require_relative '../lib/cache'

class CacheTest < Minitest::Test
  def setup
    @subject ||= Cache.new
  end

  def test_initialize
    assert @subject
    assert_equal [], @subject.cache
  end

  def meter_message(power:, time: nil)
    MeterMessage.new "{\"time\":#{time.to_i},\"latest\":{\"power_w\":\"#{power}\"}}"
  end

  def test_diff_considered_diff_when_not_enough_to_compare_to
    (Cache::MIN - 1).times do
      @subject.add meter_message(power: 1)
      assert @subject.diff?
    end

    @subject.add meter_message(power: 1)
    refute @subject.diff?
  end

  def test_diff_with_different_values
    11.times do |power|
      @subject.add meter_message power: power
    end

    assert_equal 11, @subject.count
    assert true, @subject.diff?
  end

  def test_diff_with_same_values
    11.times { @subject.add meter_message(power: 7) }

    assert_equal 11, @subject.count
    refute @subject.diff?
  end

  def test_shrink_cache!
    100.times { @subject.add meter_message(power: 1) }
    @subject.shrink_cache!
    assert_equal 100, @subject.count

    @subject.add meter_message(power: 1337)
    assert_equal 101, @subject.count

    @subject.shrink_cache!
    assert_equal Cache::SHRINK_TO, @subject.count
  end

  def test_add_rejects_empty_values
    @subject.add meter_message(power: nil)
    @subject.add meter_message(power: '')
    assert_equal 0, @subject.count

    @subject.add meter_message(power: 0)
    assert_equal 1, @subject.count
  end

  def test_add_prepends_new_values
    @subject.add meter_message(power: 1337)
    @subject.add meter_message(power: 420)
    @subject.add meter_message(power: 69)

    assert_equal 69, @subject.first.power
    assert [69, 420, 1337] == @subject.cache.map(&:power)
  end
end
