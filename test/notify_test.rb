# frozen_string_literal: true

require 'minitest/autorun'
require_relative '../lib/notify'

class NotifyTest < Minitest::Test
  def subject
    @notify ||= Notify.new
  end

  def test_throttled_never_when_fresh
    refute Notify.new.throttled?
  end

  def test_throttled_in_timeout?
    subject.update_last_message_at! time: Time.now - 100
    assert subject.throttled?
  end

  def test_throttled_past_timeout?
    subject.update_last_message_at! time: Time.now - 901
    refute subject.throttled?
  end
end
