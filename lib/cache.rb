# frozen_string_literal: true

require_relative 'meter_message'

class Cache
  MIN = 10
  MAX = 100
  SHRINK_TO = 20

  attr_accessor :cache

  def initialize
    self.cache = []
  end

  def add(value)
    return unless [:power, :time, :nil?].all? { |meth| value.respond_to? meth }
    return if value.nil?

    cache.unshift value
  end

  def shrink_cache!
    return unless count > MAX

    self.cache = cache.take SHRINK_TO
  end

  def diff?
    return true if count < MIN
    shrink_cache!

    cache[1..MIN].any? { |element| first.power != element.power }
  end

  def dump
    puts "Previous power values:"
    puts cache.take(MIN)
  end

  def first
    cache[0]
  end

  def count
    cache.count
  end
end

