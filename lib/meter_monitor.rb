# frozen_string_literal: true

require 'mqtt'

require_relative 'cache'
require_relative 'meter_message'
require_relative 'notify'

class MeterMonitor
  class NetworkException < Exception; end

  MQTT_HOST = ENV.fetch 'MQTT_HOST', 'localhost'
  MQTT_USER = ENV.fetch 'MQTT_USER', nil
  MQTT_PASSWORD = ENV.fetch 'MQTT_PASSWORD', nil
  MQTT_TOPIC = ENV.fetch 'MQTT_TOPIC', 'stats/meter'

  attr_accessor :cache, :notification_service

  def initialize
    self.cache = Cache.new
    self.notification_service = Notify.new
  end

  def monitor_power_meter
    poll_mqtt
  rescue Errno::ECONNREFUSED, SocketError, MQTT::ProtocolException => e
    notification_service.execute message: "Unexpected shutdown of monitoring\n#{e}"
    raise NetworkException.new("Unexpected Network Error")
  end

  def poll_mqtt
    MQTT::Client.connect(host: host, username: user, password: password ) do |connection|
      connection.get(topic) do |topic, message|
        process_message message
      end
    end
  end

  def process_message(message)
    meter_message = MeterMessage.new message

    puts meter_message

    cache.add meter_message

    notify meter_message unless cache.diff?
    puts "Previous power values:\n #{cache.dump}" if debug?
  end

  def notify(meter_message)
    notification_service.execute message: "Discovergy: Meter shows no change since #{meter_message.time}"
  end

  def host
    MQTT_HOST
  end

  def topic
    MQTT_TOPIC
  end

  def user
    MQTT_USER
  end

  def password
    MQTT_PASSWORD
  end

  def debug?
    ENV.fetch 'DEBUG', false
  end
end

