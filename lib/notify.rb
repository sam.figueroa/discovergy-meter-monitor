# frozen_string_literal: true

require 'net/https'
require 'date'
require 'dotenv/load'

class Notify
  PUSHOVER_TOKEN = ENV.fetch 'PUSHOVER_TOKEN'
  PUSHOVER_USER = ENV.fetch 'PUSHOVER_USER'
  PUSHOVER_URI = 'https://api.pushover.net/1/messages.json'
  PUSHOVER_PRIORITY_HIGH = 1
  PUSHOVER_SOUND = 'persistent'

  MESSAGE = 'Check meter, no new values reported'
  TITLE = 'Discovergy Monitoring'
  EPOCH_BEGINS = Time.at 0
  TIMEOUT = 15 * 60 # 15 minutes

  attr_accessor :last_message_at

  def initialize
    self.last_message_at = EPOCH_BEGINS
  end

  def execute(message: MESSAGE, title: TITLE)
    return if throttled?

    puts "[#{Time.now}] Notify: #{message}"
    update_last_message_at!
    send_message message, title
  end

  def send_message(message, title)
    res = Net::HTTP.new(url.host, url.port)
    res.use_ssl = true
    res.verify_mode = OpenSSL::SSL::VERIFY_PEER
    res.start {|http| http.request request(message, title) }
  end

  def throttled?
    Time.now - last_message_at < TIMEOUT
  end

  def update_last_message_at!(time: Time.now)
    self.last_message_at = time
  end

  def url
    @url ||= URI.parse PUSHOVER_URI
  end

  def request(message, title)
    req = Net::HTTP::Post.new(url.path)
    req.set_form_data({
      :token => PUSHOVER_TOKEN,
      :user => PUSHOVER_USER,
      :message => message,
      :title => TITLE,
      :sound => PUSHOVER_SOUND,
      :priority => PUSHOVER_PRIORITY_HIGH
    })

    req
  end

  def debug?
    ENV.fetch 'DEBUG', false
  end
end

if __FILE__ == $0 && ENV.fetch('DEBUG', false)
  Notify.new.execute message: "Notify test"
end

