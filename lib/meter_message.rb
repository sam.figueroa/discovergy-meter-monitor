# frozen_string_literal: true

require 'date'
require 'json'
require 'hashie'

class MeterMessage
  DEBUG = ENV.fetch 'DEBUG', false

  class MessageHash < Hashie::Mash
  end

  attr_accessor :message

  def initialize(encoded_json)
    msg = JSON.parse encoded_json
    self.message = MessageHash.new msg

    puts message if DEBUG
  end

  def power
    raw_power.to_i
  end

  def time
    Time.at (message.time.to_i / 1000).to_i
  end

  def to_s
    "[#{time}] Current power: #{power} Watts\n"
  end

  def raw_power
    message.latest.power_w
  end

  def nil?
    raw_power.nil? || raw_power.to_s.chomp.strip == ''
  end
end

