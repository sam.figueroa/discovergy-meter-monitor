# Why tho?

My Discovergy Smart Meter often looses stops reporting data. This is annoying when I want to take advantage of cheap electricity prices, since our rates are dynamic.

I wanted a way to be aware that the meter needs a manual reset, so this is what I came up with for now.


## Seem a bit complicated no?

I'm reading the power values from an MQTT server. That way I can be somewhat flexible on where the Discovergy data is coming from. Right now I'm reading their API with a NodeRed instance that's piping it into a Mosquitto MQTT server. 

This script listens for incoming MQTT messages from the power meter and triggers an alert when it detects that the power values aren't changing. So far I've noticed this behavior when the meter stopped reporting. If that heuristic doesn't pan I out I plan on adding timeouts to the monitoring class.

I'm pushing the notifications to my phone with [PushOver](https://pushover.net). It has a very straight forward API and awesome pricing & features.


# Feedback / Contributions

I'm very open to improvements to my approach. Have at it.
